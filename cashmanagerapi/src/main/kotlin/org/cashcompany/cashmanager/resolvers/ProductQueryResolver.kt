package org.cashcompany.cashmanager.resolvers


import org.cashcompany.cashmanager.repository.ProductRepository
import org.cashcompany.cashmanager.entity.Product
import org.cashcompany.cashmanager.entity.Review
import org.cashcompany.cashmanager.entity.Price


import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class ProductQueryResolver(val productRepository: ProductRepository,
                         private val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun products(): List<Product> {
        val list = productRepository.findAll()
        for (item in list) {
            item.reviews = getReviews(productId = item.id)
            item.prices = getPrices(productId = item.id)
        }
        return list
    }

    private fun getReviews(productId: String): List<Review> {
        val query = Query()
        query.addCriteria(Criteria.where("productId").`is`(productId))
        return mongoOperations.find(query, Review::class.java)
    }

    private fun getPrices(productId: String): List<Price> {
        val query = Query()
        query.addCriteria(Criteria.where("productId").`is`(productId))
        return mongoOperations.find(query, Price::class.java)
    }
}