package org.cashcompany.cashmanager.resolvers 

import org.cashcompany.cashmanager.entity.Product
import org.cashcompany.cashmanager.repository.ProductRepository
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class ProductMutationResolver (private val productRepository: ProductRepository): GraphQLMutationResolver {
    fun newProduct(name: String, amount: Float): Product {
        val product = Product(name, amount)
        product.id = UUID.randomUUID().toString()
        productRepository.save(product)
        return product
    }

    fun deleteProduct(id:String): Boolean {
        productRepository.deleteById(id)
        return true
    }

    fun updateProduct(id:String, amount:Float): Product {
        val product = productRepository.findById(id)
        product.ifPresent {
            it.amount = amount
            productRepository.save(it)
        }
        return product.get()
    }
}

