package org.cashcompany.cashmanager.resolvers

import org.cashcompany.cashmanager.entity.Price

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component

@Component
class PriceQueryResolver(val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun prices(productId: String): List<Price> {
        val query = Query()
        query.addCriteria(Criteria.where("productId").`is`(productId))
        return mongoOperations.find(query, Price::class.java)
    }
}