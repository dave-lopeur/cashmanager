package org.cashcompany.cashmanager.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.cashcompany.cashmanager.entity.Review
import org.cashcompany.cashmanager.repository.ReviewRepository
import org.springframework.stereotype.Component

@Component
class ReviewMutationResolver(private val reviewRepository: ReviewRepository) : GraphQLMutationResolver {
    fun newReview(productId: String, rating: Int, text: String): Review {
        val review = Review(productId, rating, text)
        reviewRepository.save(review)
        return review
    }
}
