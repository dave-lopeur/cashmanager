package org.cashcompany.cashmanager.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.cashcompany.cashmanager.entity.Price
import org.cashcompany.cashmanager.repository.PriceRepository
import org.springframework.stereotype.Component

@Component
class PriceMutationResolver(private val priceRepository: PriceRepository) : GraphQLMutationResolver {
    fun newPrice(productId: String, amount: Float, unit: String, currency: String): Price {
        val price = Price(productId, amount, unit, currency)
        priceRepository.save(price)
        return price
    }
}

