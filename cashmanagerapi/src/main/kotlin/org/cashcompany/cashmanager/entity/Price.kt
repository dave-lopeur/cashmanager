package org.cashcompany.cashmanager.entity

import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "prices")
@CompoundIndex(def = "{'productId':1, 'amount': 1, 'unit': 1, 'currency': 1}", unique = true, sparse = true)
data class Price(
    var productId: String,
    var amount: Float,
    var unit: String,
    var currency: String
)

