package org.cashcompany.cashmanager.entity


import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.index.Indexed


@Document(collection = "products")
data class Product(
        var name: String,
        var amount: Float
) {
    @Id
    var id: String = ""

    @Transient
    var reviews: List<Review> = ArrayList()

    @Transient
    var prices: List<Price> = ArrayList()
}