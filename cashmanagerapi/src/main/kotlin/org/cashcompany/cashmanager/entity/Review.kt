package org.cashcompany.cashmanager.entity

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "reviews")
data class Review(
        var productId: String,
        var rating: Int,
        var text: String
)