package org.cashcompany.cashmanager.repository

import org.cashcompany.cashmanager.entity.Price
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface PriceRepository : MongoRepository<Price, String>