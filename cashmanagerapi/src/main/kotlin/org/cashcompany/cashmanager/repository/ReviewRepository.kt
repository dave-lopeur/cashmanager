package org.cashcompany.cashmanager.repository

import org.cashcompany.cashmanager.entity.Review
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ReviewRepository : MongoRepository<Review, String>