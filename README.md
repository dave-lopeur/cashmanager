# Cash Manager
A fast payment processor


### CashManager API

Written in Kotlin with the help of the Spring Boot Framework,
it present various (unprotected) GraphQl resources*  :


- The GraphQl editor on http://localhost:9000/graphiql
- The GraphQl endpoint on http://localhost:9000/graphql

\*after a `docker-compose up`

### CashManager App (terminal)

Written in Dart with the help of the Flutter framework, it present 
a graphical user interface to interact with the CashManager API.

### Project build :

Be sure to have docker and docker-compose installed then run :

```
docker-compose up
```