import 'package:cashmanagerapp/checkout_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class ProductList extends StatelessWidget {
  const ProductList({
    Key key,
    @required this.readProducts,
  }) : super(key: key);

  final String readProducts;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0, left: 8.0, top: 25),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(),
              ),
              Text(
                "Qtt : 0",
                textScaleFactor: 1.8,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Total : 0 €",
                textScaleFactor: 1.8,
              ),
              Expanded(
                child: Container(),
              ),
              RaisedButton(
                child: Text("Order Now"),
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (ctx) => CheckoutPage())),
              )
            ],
          ),
        ),
        Expanded(
          child: Query(
            options: QueryOptions(document: readProducts, pollInterval: 10),
            builder: (QueryResult result,
                {VoidCallback refetch, FetchMore fetchMore}) {
              if (result.errors != null) {
                return Text(result.errors.toString());
              }

              if (result.loading) {
                return Text('Loading');
              }

              // it can be either Map or List
              List repositories = result.data['products'];

              return ListView.builder(
                  itemCount: repositories.length,
                  itemBuilder: (context, index) {
                    final repository = repositories[index];
                    return Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          DecoratedBox(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: NetworkImage(
                                    "https://www.apple.com/v/macbook-pro/o/images/shared/tile_macos_large.png"),
                              )),
                              child: SizedBox(
                                width: 200,
                                height: 200,
                                child: Center(
                                  child: Icon(
                                    Icons.zoom_in,
                                    color: Colors.white,
                                  ),
                                ),
                              )),
                          Card(
                            elevation: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.panorama_fish_eye),
                                    Text(repository['name'])
                                  ],
                                ),
                                repository['prices'] != [] &&
                                        List.from(repository['prices']).length >
                                            0
                                    ? Row(
                                        children: <Widget>[
                                          Icon(Icons.euro_symbol),
                                          Text(repository['prices'][0]['amount']
                                                  .toString() +
                                              " " +
                                              repository['prices'][0]
                                                  ['currency'])
                                        ],
                                      )
                                    : Container(),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.info),
                                    Text(
                                      "Ref : " +
                                          repository['id']
                                              .toString()
                                              .split('-')[0],
                                      textScaleFactor: 1,
                                    )
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.add),
                                    Text(
                                      repository['amount'] != null
                                          ? "In stock (" +
                                              repository['amount'].toString() +
                                              " left)"
                                          : "x",
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 10.0, left: 10.0),
                                  child: FlatButton.icon(
                                    autofocus: true,
                                    icon: Icon(Icons.shopping_basket),
                                    onPressed: () =>
                                        print("buy_now " + repository['id']),
                                    label: Text("Add one"),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  });
            },
          ),
        ),
      ],
    );
  }
}
