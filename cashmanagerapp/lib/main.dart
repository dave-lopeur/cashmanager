import 'package:cashmanagerapp/payments_page.dart';
import 'package:cashmanagerapp/productlist_widget.dart';
import 'package:cashmanagerapp/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

void main() {
  final HttpLink httpLink = HttpLink(
    uri: 'http://localhost:9000/graphql',
  );
  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: InMemoryCache(),
      link: httpLink,
    ),
  );
  runApp(MyApp(
    client: client,
  ));
}

class MyApp extends StatelessWidget {
  final ValueNotifier<GraphQLClient> client;

  MyApp({this.client});

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          brightness: Brightness.dark,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var index = 0;
  String readProducts = """
  query {
  products {
    name
    id
    amount
    prices {
      currency
      amount
      unit
    }
  }
}
""";
  @override
  Widget build(BuildContext context) {
    final List<Widget> _pages = [
      ProductList(
        readProducts: readProducts,
      ),
      PaymentPage(),
      SettingsPage()
    ];
    return Scaffold(
      body: _pages[index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: (i) => setState(() {
          index = i;
        }),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.shop), title: Text("Products")),
          BottomNavigationBarItem(
              icon: Icon(Icons.credit_card), title: Text("Payments")),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), title: Text("Settings")),
        ],
      ),
    );
  }
}

