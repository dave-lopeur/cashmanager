import 'package:flutter/material.dart';

class CheckoutPage extends StatefulWidget {
  CheckoutPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          children: <Widget>[
            Text("Your cart"),
            Text(
              "0 items",
              textScaleFactor: 0.7,
            )
          ],
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Center(
                child: Card(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.payment, size: 50),
                      Icon(Icons.scanner, size: 50)
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Center(
                child: Card(
                  child: Row(
                    children: <Widget>[Icon(Icons.check, size: 50)],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
