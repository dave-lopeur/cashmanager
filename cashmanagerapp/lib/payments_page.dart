import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';

class PaymentPage extends StatefulWidget {
  PaymentPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Center(
                child: Icon(Icons.save, size: 50),
              ),
              CreditCardWidget(
                cardNumber: "534102567856432",
                expiryDate: "10/03/2086",
                cardHolderName: "MR JOHN DO.",
                cvvCode: "334",
                showBackView: false, //true when you want to show cvv(back) view
              ),
              Expanded(
                child: Container(),
              ),
              FloatingActionButton(
                onPressed: () => print("add_payment"),
                child: Icon(Icons.add),
              )
            ],
          ),
        ),
      ),
    );
  }
}
