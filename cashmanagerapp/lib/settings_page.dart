import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    final serverField = TextField(
      obscureText: false,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Server address",
          icon: Icon(Icons.http),
          // border:
          //     OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
              ),
    );
    final adminIdField = TextField(
      obscureText: false,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          icon: Icon(Icons.supervised_user_circle),
          hintText: "Admin ID",
          // border:
          //     OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
              ),
    );
    final passwordField = TextField(
      obscureText: true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          icon: Icon(Icons.lock),
          // border:
          //     OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
              ),
    );
    return Card(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.center,
            dragStartBehavior: DragStartBehavior.down,
            children: <Widget>[
              SizedBox(height: 35.0),
              Icon(Icons.lock_open, size: 67,),
              SizedBox(height: 35.0),
              serverField,
              SizedBox(height: 15.0),
              adminIdField,
              SizedBox(height: 25.0),
              passwordField,
              SizedBox(
                height: 25.0,
              ),
              FlatButton.icon(
                icon: Icon(Icons.refresh),
                label: Text("Connect"),
                onPressed: () => print("connect"),
              ),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
